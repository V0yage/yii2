<?php


namespace app\models;


use yii\base\Model;

class EntryForm extends Model
{
    public $name;
    public $email;
    public $text;
    public $topic;

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'text' => 'Текст сообщения',
            'topic' => 'Тема сообщения'
        ];
    }

    public function rules()
    {
        return [
            [['name', 'email', 'text', 'topic'], 'required'],
            ['email', 'email'],
            ['topic', 'string', 'min' => 3],
            ['topic', 'string', 'max' => 5],
            #['topic', 'safe']
        ];
    }
}