<?php


namespace app\assets;


use yii\web\AssetBundle;

class TestAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    #public $sourcePath = '@app/components';
    public $css = [
        #'css/styles.css'
    ];
    public $js = [
        #'js/scripts.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}