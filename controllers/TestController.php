<?php


namespace app\controllers;

use app\models\Country;
use yii\web\Controller;

class TestController extends Controller
{
    public $var;
    #public $layout = 'test';

    public function actionIndex($name = 'Gustavo', $age = 17)
    {
        debug(\Yii::getAlias('@webroot'));
        debug(\Yii::getAlias('@web'));

        $this->layout = 'test';

        $this->var = 'Test controller variable';    #in view getting as $this->context->my_var
        \Yii::$app->view->params['val'] = 'Global param';
        $this->view->title = 'Page title';

        return $this->render('index', compact(['name', 'age']));
    }

    public function actions()
    {
        return [
            'test' => 'app\components\HelloAction'
        ];
    }

    public function actionMyTest()
    {
        $this->view->title = 'Page title';
        return $this->render('my-test');
    }

    public function actionView()
    {
        $this->layout = 'test';
        $this->view->title = 'Active Record';

        $model = new Country();

        return $this->render('view', compact('model'));
    }
}