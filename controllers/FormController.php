<?php

namespace app\controllers;

use app\models\EntryForm;
use yii\web\Controller;

class FormController extends Controller
{
    public $layout = 'form';

    public function actionIndex()
    {
        $this->view->title = 'Страница с формой';

        $model = new EntryForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            return $this->refresh();
            /*if (\Yii::$app->request->isPjax) {
                \Yii::$app->session->setFlash('success', 'Данные приняты через Pjax');
                $model = new EntryForm();
            } else {
                \Yii::$app->session->setFlash('success', 'Данные приняты стандартно');
                return $this->refresh();
            }*/
        }

        return $this->render('index', compact('model'));
    }
}