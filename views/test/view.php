<?php
    use \yii\widgets\ActiveForm;
?>

<div class="col-md-12">
    <h1>Active Record</h1>
    <?php debug($model->getAttributes()) ?>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'code') ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'population') ?>
    <?= $form->field($model, 'status') ?>
    <?php ActiveForm::end() ?>
</div>